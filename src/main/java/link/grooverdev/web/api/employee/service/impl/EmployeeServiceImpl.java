package link.grooverdev.web.api.employee.service.impl;

import link.grooverdev.web.api.employee.entity.Employee;
import link.grooverdev.web.api.employee.exception.EmployeeNotFoundException;
import link.grooverdev.web.api.employee.repository.EmployeeRepository;
import link.grooverdev.web.api.employee.service.IEmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 22:03
 * @project back-employee
 **/
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements IEmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAllByOrderByEmployeeIdAsc();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> getEnabledEmployees(boolean enabled) {
        return employeeRepository.findAllByEnabled(enabled);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Employee> getEmployeeByID(String id) {
        return employeeRepository.findByEmployeeId(id);
    }

    @Override
    @Transactional
    public Employee save(Employee newEmployee) {
        return employeeRepository.save(newEmployee);
    }

    @Override
    @Transactional
    public Employee update(String id, Employee updEmployee) {

        var updatedEmployee = employeeRepository.findByEmployeeId(id);

        if (updatedEmployee.isPresent()) {
            updatedEmployee.get().setFirstName(updEmployee.getFirstName());
            updatedEmployee.get().setLastName(updEmployee.getLastName());
            updatedEmployee.get().setEmailId(updEmployee.getEmailId());
            updatedEmployee.get().setEnabled(updEmployee.isEnabled());
        }
        return employeeRepository.save(updatedEmployee.get());
    }

    @Override
    @Transactional
    public Employee delete(String id) {

        var deletedEmployee = employeeRepository.findByEmployeeId(id).orElseThrow(()-> new EmployeeNotFoundException(id, "/api/v1/employees/" + id));
        deletedEmployee.setEnabled(false);
        return employeeRepository.save(deletedEmployee);
    }
}
