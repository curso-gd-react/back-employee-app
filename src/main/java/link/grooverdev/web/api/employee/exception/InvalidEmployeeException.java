package link.grooverdev.web.api.employee.exception;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 26/10/2024 - 22:11
 * @project back-employee
 **/
public class InvalidEmployeeException extends RuntimeException  {

    public InvalidEmployeeException(String message) {
        super(message);
    }
}
