package link.grooverdev.web.api.employee.service;

import link.grooverdev.web.api.employee.entity.Employee;

import java.util.List;
import java.util.Optional;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 22:00
 * @project back-employee
 **/
public interface IEmployeeService {

    List<Employee> getAllEmployees();
    List<Employee> getEnabledEmployees(boolean enabled);
    Optional<Employee> getEmployeeByID(String id);
    Employee save(Employee newEmployee);
    Employee update(String id, Employee updEmployee);
    Employee delete(String id);
}
