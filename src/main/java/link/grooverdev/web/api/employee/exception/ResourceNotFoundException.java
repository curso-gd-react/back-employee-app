package link.grooverdev.web.api.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 23:33
 * @project back-employee
 **/
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
