/*Table User*/
CREATE TABLE employee.employees(
    employee_id UUID PRIMARY KEY DEFAULT uuid_generate_v7() UNIQUE NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email_id text NOT NULL,
    enabled boolean NOT NULL default true,
    created_by text NOT NULL,
    created_date TIMESTAMP NOT NULL,
    modified_by text NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,
    count_modified bigint NOT NULL
);

alter table employee.employees alter column last_modified_date type bytea using PGP_SYM_ENCRYPT(last_modified_date::text, current_setting('encrypt.key'));
alter table employee.employees alter column created_date type bytea using PGP_SYM_ENCRYPT(created_date::text, current_setting('encrypt.key'));
