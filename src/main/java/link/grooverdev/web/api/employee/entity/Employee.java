package link.grooverdev.web.api.employee.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import link.grooverdev.web.api.employee.audit.Auditable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.UuidGenerator;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 21:50
 * @project back-employee
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employees", schema = "employee")
public class Employee extends Auditable {

    @Id
    @UuidGenerator
    @Column(name = "employee_id", nullable = false, unique = true)
    private String employeeId;

    @ColumnTransformer(read = "pgp_sym_decrypt(first_name::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(name = "first_name", length = 50)
    private String firstName;

    @ColumnTransformer(read = "pgp_sym_decrypt(last_name::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @ColumnTransformer(read = "pgp_sym_decrypt(email_id::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(name = "email_id", length = 50)
    private String emailId;

    private boolean enabled;
}
