package link.grooverdev.web.api.employee.controller;

import jakarta.validation.Valid;
import link.grooverdev.web.api.employee.dto.EmployeeDto;
import link.grooverdev.web.api.employee.entity.Employee;
import link.grooverdev.web.api.employee.exception.EmployeeNotFoundException;
import link.grooverdev.web.api.employee.service.IEmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 22:12
 * @project back-employee
 **/
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
@RestController
@Slf4j
public class EmployeeController {

    private final IEmployeeService employeeService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping("employees")
    public ResponseEntity<List<EmployeeDto>> getAllEmployees() {

        try {
            var employees = employeeService.getAllEmployees().stream()
                    .map(allHtl-> modelMapper.map(allHtl, EmployeeDto.class)).collect(Collectors.toList());

            if (employees.isEmpty()) {
                log.debug("primera ejecución del programa");
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else {
                log.debug("Listado completo de registros");
                return new ResponseEntity<>(employees, HttpStatus.OK);
            }
        }
        catch (DataAccessException daex) {
            log.error("Error al obtener el listado de registros " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("enabled-employees/{enabled}")
    public ResponseEntity<List<EmployeeDto>> findEnableEmployees(@Valid boolean enabled) {

        try {
            var enabledEmployees = employeeService.getEnabledEmployees(true).stream()
                    .map(enabHtl-> modelMapper.map(enabHtl, EmployeeDto.class)).collect(Collectors.toList());
            log.debug("Listado de registros habilitados");
            return new ResponseEntity<>(enabledEmployees, HttpStatus.OK);
        }
        catch (DataAccessException daex) {
            log.error("error al obtener el listado de registros habilitados " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("employees/{id}")
    public ResponseEntity<EmployeeDto> getHotelByID(@Valid @PathVariable String id) {

        try {
            var singleEmployee =  employeeService.getEmployeeByID(id).orElseThrow(()-> new EmployeeNotFoundException(id, "/api/v1/employees/" + id));
            var singleEmployeeResponse = modelMapper.map(singleEmployee, EmployeeDto.class);

            if (singleEmployeeResponse != null) {
                log.debug("Un registro");
                return new ResponseEntity<>(singleEmployeeResponse, HttpStatus.OK);
            }
        }
        catch (DataAccessException daex) {
            log.error("Ocurriò un error al obtener un registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("employees")
    public ResponseEntity<EmployeeDto> saveEmployee(@Valid @RequestBody EmployeeDto newEmployeeDto) {

        try {
            var requestEmployee = modelMapper.map(newEmployeeDto, Employee.class);
            var savedEmployee = employeeService.save(requestEmployee);
            var responseEmployee = modelMapper.map(savedEmployee, EmployeeDto.class);

            log.debug("Registro guardado de forma exitosa");
            return new ResponseEntity<>(responseEmployee, HttpStatus.CREATED);
        }
        catch (DataAccessException daex) {
            log.error("Ocurriò un error al guardar un registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("employees/{id}")
    public ResponseEntity<EmployeeDto> updateEmployee(@Valid @PathVariable String id, @Valid @RequestBody EmployeeDto updEmployeeDto) {

        try {
            var requestEmployee = modelMapper.map(updEmployeeDto, Employee.class);
            var updatedEmployee = employeeService.update(id, requestEmployee);
            var responseEmployee = modelMapper.map(updatedEmployee, EmployeeDto.class);

            log.debug("Registro actualizado de forma exitosa");
            return new ResponseEntity<>(responseEmployee, HttpStatus.ACCEPTED);
        }
        catch (DataAccessException daex) {
            log.error("Ocurriò un error al actualizar el registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PatchMapping("employees/{id}")
    public ResponseEntity<EmployeeDto> deleteEmployeeByID(@Valid @PathVariable String id) {

        try {
            var deletedEmployee = employeeService.delete(id);
            var deletedEmployeeResponse = modelMapper.map(deletedEmployee, EmployeeDto.class);

            log.debug("Registro eliminado de forma exitosa");
            return new ResponseEntity<>(deletedEmployeeResponse, HttpStatus.NO_CONTENT);
        }
        catch (DataAccessException daex) {
            log.error("Ocurriò un error al eliminar el registro " + daex.getMostSpecificCause());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
