package link.grooverdev.web.api.employee.repository;

import link.grooverdev.web.api.employee.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 21:56
 * @project back-employee
 **/
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findAllByOrderByEmployeeIdAsc();
    List<Employee> findAllByEnabled(boolean enabled);
    Optional<Employee> findByEmployeeId(String employeeId);
}
