package link.grooverdev.web.api.employee.audit;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnTransformer;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 22:52
 * @project back-employee
 **/
@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder
public class Auditable {

    @CreatedBy
    @ColumnTransformer(read = "pgp_sym_decrypt(created_by::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @CreatedDate
    @ColumnTransformer(read = "pgp_sym_decrypt(created_date::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    private LocalDateTime createdDate;

    @LastModifiedBy
    @ColumnTransformer(read = "pgp_sym_decrypt(modified_by::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(name = "modified_by")
    private String modifiedBy;

    @LastModifiedDate
    @ColumnTransformer(read = "pgp_sym_decrypt(last_modified_date::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(nullable = false)
    private LocalDateTime lastModifiedDate;

    @Version
    @ColumnTransformer(read = "pgp_sym_decrypt(count_modified::bytea,current_setting('encrypt.key'))", write = "pgp_sym_encrypt(?,current_setting('encrypt.key'))")
    @Column(name = "count_modified")
    private long countModified;

}
