package link.grooverdev.web.api.employee.dto;

import lombok.Data;
import java.time.LocalDateTime;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 27/04/2024 - 22:10
 * @project back-employee
 **/
@Data
public class EmployeeDto {

    private String employeeId;
    private String firstName;
    private String lastName;
    private String emailId;
    private boolean enabled;
    private String createdBy;
    private LocalDateTime createdDate;
    private String modifiedBy;
    private LocalDateTime lastModifiedDate;
    private long countModified;
}
