package link.grooverdev.web.api.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.ErrorResponseException;

import java.net.URI;
import java.time.Instant;

/**
 * @author grooverdev
 * © COPYRIGHT Gr00v3rD3v-GD
 * @created 26/10/2024 - 22:08
 * @project back-employee
 **/
public class EmployeeNotFoundException extends ErrorResponseException {

    public EmployeeNotFoundException(String userId, String path) {
        super(HttpStatus.NOT_FOUND, problemDetailFrom("User with id " + userId + " not found", path), null);
    }

    private static ProblemDetail problemDetailFrom(String message, String path) {

        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, message);
        problemDetail.setType(URI.create("http://localhost:9009/errors/not-found"));
        problemDetail.setTitle("Employee not found");
        problemDetail.setInstance(URI.create(path));
        problemDetail.setProperty("timestamp", Instant.now()); // additional data
        return problemDetail;
    }
}
